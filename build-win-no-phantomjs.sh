#!/bin/bash
rm -rf rechat_recorder_win
mkdir -p rechat_recorder_win/res

PKG_CONFIG=x86_64-w64-mingw32-pkg-config \
PKG_CONFIG_ALLOW_CROSS=1 \
cargo build --release --target 'x86_64-pc-windows-gnu' || exit 1

cp -r res COPYING target/x86_64-pc-windows-gnu/release/rechat-record.exe rechat_recorder_win
x86_64-w64-mingw32-strip rechat_recorder_win/rechat-record.exe

rm rechat_recorder_win/res/settings.json
cat > rechat_recorder_win/res/settings.json <<EOF
{
    "width"      : 640,
    "height"     : 340,
    "fps"        : 25,
    "output_dir" : ".",

    "render_alpha"   : false,

    "phantomjs_path" : "phantomjs-2.1.1-windows/bin/phantomjs.exe"
}
EOF
unix2dos rechat_recorder_win/res/settings.json

rm rechat_recorder_win/res/userstyle.css
cat > rechat_recorder_win/res/userstyle.css <<EOF
/* user style overrides */

body {
    background-color: #0F0F0F;
}

.message {
    color: #FFFFFF;
}

.message-deleted {
    color: #898395;
}
EOF
unix2dos rechat_recorder_win/res/userstyle.css

rm rechat_recorder_win.zip
7z a rechat_recorder_win.zip rechat_recorder_win
