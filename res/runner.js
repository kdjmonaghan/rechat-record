/*
 * Copyright (c) 2017 hexpointer <hexptr@gmx.com>
 *
 * This file is part of rechat-record.

 * rechat-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * rechat-record is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with rechat-record.  If not, see <http://www.gnu.org/licenses/>.
 */

try {

var page = require('webpage').create();
var system = require('system');
var fs = require('fs');

if (system.args.length !== 3) {
  system.stderr.writeLine("usage: runner.js <width> <height>");
  phantom.exit(1);
}

var _w = parseInt(system.args[1], 10);
var _h = parseInt(system.args[2], 10);

page.viewportSize = { width: _w, height: _h };
page.clipRect = { width: _w, height: _h };

var badgesJSON = JSON.parse(fs.read("res/badges.json"));
var bttvChannelJSON = JSON.parse(fs.read("res/bttv_channel.json"));
var bttvEmotesJSON = JSON.parse(fs.read("res/bttv_emotes.json"));
var channelFFZEmotesJSON = JSON.parse(fs.read("res/channel_ffz_emotes.json"));
var ffzEmotesJSON = JSON.parse(fs.read("res/global_ffz_emotes.json"));

var _badges = badgesJSON.badge_sets;

var _bttvEmotes = {};
[bttvEmotesJSON, bttvChannelJSON].forEach(function (json) {
  json.emotes.forEach(function (emote) {
    _bttvEmotes[emote.code] = emote.id;
  });
});

var _channelFFZEmotes = {};
[channelFFZEmotesJSON].forEach(function (json) {
    var set = json.room.set;

    json.sets[set].emoticons.forEach(function (emote) {
        var emoteName = emote.name;
        var catchFile = new RegExp('[^/]+$');
        var url = catchFile.exec(emote.urls[2]);
        var icon = "emoticon";

        if (url == "undefined") {
            url = catchFile.exec(emote.urls[1]);
            icon = "emoticon-big";
        }

        _channelFFZEmotes[emoteName] = {
            "url":url[0],
            "icon":icon
        }
    });
});

var _ffzEmotes = {};
[ffzEmotesJSON].forEach(function (json) {
    var set = json.default_sets[0];

    json.sets[set].emoticons.forEach(function (emote) {
        var emoteName = emote.name;
        var catchFile = new RegExp('[^/]+$');
        var url = catchFile.exec(emote.urls[2]);
        var icon = "emoticon";

        if (url == "undefined") {
            url = catchFile.exec(emote.urls[1]);
            icon = "emoticon-big";
        }

        _ffzEmotes[emoteName] = {
            "url":url[0],
            "icon":icon
        }
    });
});


function finishUp(retCode) {
  var _nerrors = page.evaluate(function () { return _nerrors; });
  var _errors = page.evaluate(function () { return _errors; });
  if (_nerrors > 0) {
    system.stderr.writeLine("runner.js makeMessageNode skipped " + _nerrors + " messages!");
  }
  for (var msg in _errors) {
    system.stderr.writeLine("runner.js makeMessageNode exception: " + msg);
    system.stderr.writeLine("triggered by the following message:");
    system.stderr.writeLine(JSON.stringify(_errors[msg]));
  }
  phantom.exit(retCode);
}

var recursionCount = 0;
function processNextMsg() {
  try {
    system.stdout.writeLine(page.renderBase64('PNG'));

    var msgString = system.stdin.readLine();
    var msg = JSON.parse(msgString);
    if (msg === null) {
      return finishUp(0);
    }

    var pageFun = function (msg) {
      var container = document.getElementById('container');
      makeMessageNode(msg, function (node) {
        if (node !== null) {
          if (container.childNodes.length >= _max_messages) {
            container.removeChild(container.firstChild);
          }
          container.appendChild(node);
          container.scrollTop = container.scrollHeight;
        }
        window.callPhantom();
      });
    };

    if (++recursionCount > 100) {
      // break recursion to avoid stack overflow
      recursionCount = 0;
      page.evaluateAsync(pageFun, 0, msg);
    } else {
      page.evaluate(pageFun, msg);
    }
  } catch (e) {
    system.stderr.writeLine("runner.js processNextMsg exception: " + e.message);
    system.stderr.writeLine("stacktrace:\n" + e.stack);
    finishUp(1);
  }
}

page.onCallback = processNextMsg;

page.open('res/container.html', function () {
  page.evaluate(function (_badges, _bttvEmotes, _ffzEmotes, _channelFFZEmotes, _h) {
    window._badges = _badges;
    window._bttvEmotes = _bttvEmotes;
    window._ffzEmotes = _ffzEmotes;
    window._channelFFZEmotes = _channelFFZEmotes;
    window._errors = {};
    window._nerrors = 0;
    window._max_messages = Math.ceil(_h / 40) + 1;
  }, _badges, _bttvEmotes, _ffzEmotes, _channelFFZEmotes, _h);

  processNextMsg();
});

} catch (e) {
  system.stderr.writeLine("runner.js exception: " + e);
  phantom.exit(1);
}
