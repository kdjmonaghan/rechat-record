/*
 * Copyright (c) 2017 hexpointer <hexptr@gmx.com>
 *
 * This file is part of rechat-record.

 * rechat-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * rechat-record is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with rechat-record.  If not, see <http://www.gnu.org/licenses/>.
 */

var MAX_SATURATION = 0.65;
var MIN_VALUE = 0.45;
var MAX_VALUE = 0.85;

var nicknameColors = Please.make_color({ colors_returned: 50, saturation: MAX_SATURATION, luminance: 0.55 });

var _cachedImagesDOM = {};
var _cachedColors = {};

function generateColorForNickname(nickname) {
  var hash = 0;
  for (var i = 0; i < nickname.length; i++) {
    var chr   = nickname.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  hash = Math.abs(hash);
  return nicknameColors[hash % (nicknameColors.length - 1)];
}


function makeMessageNode(msg, allDoneCb) {
  var theNode = document.createElement('div');
  theNode.classList.add("message-line");

  var nImages = 1;
  function imageCb() {
    if (--nImages === 0) {
      return allDoneCb(theNode);
    }
  }
  function makeImage(cls, id, src) {
    var imgNode = document.createElement('img');
    imgNode.src = src;
    imgNode.classList.add(cls);

    var cachedImageNode = _cachedImagesDOM[id];
    if (cachedImageNode === undefined) {
      ++nImages;
      imgNode.onload = imgNode.onerror = imageCb;
      _cachedImagesDOM[id] = imgNode;
      return imgNode;
    } else {
      return cachedImageNode.cloneNode(false);
    }
  }

  // badges
  try {
    {
      var badgesNode = document.createElement("span");
      badgesNode.classList.add("badges");
      if (msg.message.user_badges !== undefined) {
        msg.message.user_badges.forEach(function (badge) {
          var badgeNode = makeImage("badge", badge._id+"_"+badge.version, _badges[badge._id].versions[badge.version].image_url_2x);
          badgesNode.appendChild(badgeNode);
        });
      }
      theNode.appendChild(badgesNode);
      theNode.appendChild(document.createTextNode(" "));
    }

    // name
    var nameColor = null;
    {
      var nameNode = document.createElement("span");
      nameNode.classList.add("from");

      if (msg.message.user_color !== undefined) {
        // calculate hsl
        if (!(msg.message.user_color in _cachedColors)) {
          var hsv = Please.HEX_to_HSV(msg.message.user_color);

          // clamp saturation and luminance
          hsv.s = Math.min(hsv.s, MAX_SATURATION);
          hsv.v = Math.max(hsv.v, MIN_VALUE);
          hsv.v = Math.min(hsv.v, MAX_VALUE);

          _cachedColors[msg.message.user_color] = Please.HSV_to_HEX(hsv);
        }
        nameColor = _cachedColors[msg.message.user_color];
      } else {
        nameColor = generateColorForNickname(msg.commenter.name);
      }
      nameNode.style.color = nameColor;

      var name = msg.commenter.display_name;
      if (name === undefined) name = msg.commenter.name;
      nameNode.appendChild(document.createTextNode(name));

      theNode.appendChild(nameNode);
    }

    // colon
    {
      var colonNode = document.createElement("span");
      colonNode.classList.add("colon");
      colonNode.appendChild(document.createTextNode(":"));

      theNode.appendChild(colonNode);
      theNode.appendChild(document.createTextNode(" "));
    }

    // message
    {
      var theMessageNode = document.createElement("span");
      theMessageNode.classList.add("message");

      /*
      if (msg.deleted) {
        theMessageNode.classList.add("message-deleted");
        theMessageNode.appendChild(document.createTextNode("<message deleted>"));
      } else {
      */
        var processText = function (mstr) {
          var first = true;
          mstr.split(' ').forEach(function (word) {
            if (first) {
              first = false;
            } else {
              theMessageNode.appendChild(document.createTextNode(' '));
            }
            if (word in _bttvEmotes) {
              var node = makeImage('emoticon', word, 'https://cdn.betterttv.net/emote/' + _bttvEmotes[word] + '/2x');
              theMessageNode.appendChild(node);
            } else if (word in _ffzEmotes) {
              var node = makeImage(_ffzEmotes[word]['icon'], word, 'https://cdn.frankerfacez.com/' + _ffzEmotes[word]['url']);
              theMessageNode.appendChild(node);
            } else if (word in _channelFFZEmotes) {
              var node = makeImage(_channelFFZEmotes[word]['icon'], word, 'https://cdn.frankerfacez.com/' + _channelFFZEmotes[word]['url']);
              theMessageNode.appendChild(node);
            } else {
              theMessageNode.appendChild(document.createTextNode(word));
            }
          });
        }
        var processEmote = function (emoteId) {
          var node = makeImage('emoticon', emoteId, 'https://static-cdn.jtvnw.net/emoticons/v1/' + emoteId + '/2.0');
          theMessageNode.appendChild(node);
        }
        msg.message.fragments.forEach(function (frag) {
          if (frag.emoticon !== undefined) {
            processEmote(frag.emoticon.emoticon_id);
          } else {
            processText(frag.text);
          }
        });

        theMessageNode.normalize();
      /*
      }
      */

      // check for /me
      if (msg.message.is_action) {
        theNode.classList.add("action");
        theMessageNode.style.color = nameColor;
      }

      theNode.appendChild(theMessageNode);
    }

  } catch (e) {
    _nerrors += 1;
    _errors[e.message] = msg;
    return allDoneCb(null);
  }

  imageCb();
}
