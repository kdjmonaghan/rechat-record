/*
 * Copyright (c) 2017 hexpointer <hexptr@gmx.com>
 *
 * This file is part of rechat-record.

 * rechat-record is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * rechat-record is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with rechat-record.  If not, see <http://www.gnu.org/licenses/>.
 */


#![feature(thread_id)]

extern crate backtrace;
extern crate libc;
extern crate time;
extern crate imagefmt;
#[macro_use] extern crate log;
extern crate log4rs;
#[macro_use] extern crate quick_error;
#[macro_use] extern crate serde_derive;
extern crate serde_json;
extern crate rustc_serialize;
#[macro_use] extern crate hyper;
extern crate hyper_native_tls;

use std::fs::{File};
use std::path::{Path, PathBuf};
use std::sync::mpsc;
use std::iter::FromIterator;
use std::io::{BufRead, BufReader, Read, Write};
use std::process::{Command, Stdio};
use std::thread;
use rustc_serialize::base64::FromBase64;

#[derive(Clone, Deserialize)]
struct Settings {
    phantomjs_path: PathBuf,
    output_dir:     PathBuf,
    width:          u32,
    height:         u32,
    fps:            u32,
    render_alpha:   bool,
}

const CHUNK_ATTEMPTS: i32 = 6;
const CHUNK_ATTEMPT_SLEEP: u64 = 5;
const CID: &'static str = "isaxc3wjcarzh4vgvz11cslcthw0gw";

const SETTINGS_JSON: &'static str = "./res/settings.json";

quick_error! {
    #[derive(Debug)]
    pub enum Error {
        MyError(err: String) {
            description(err)
            display("{}", err)
            from()
        }
        IOError(err: std::io::Error) {
            description(err.description())
            display("{}", err)
            from()
        }
        Hyper(err: hyper::error::Error) {
            description(err.description())
            display("{}", err)
            from()
        }
        Time(err: time::ParseError) {
            description(err.description())
            display("{}", err)
            from()
        }
        Serde(err: serde_json::Error) {
            description(err.description())
            display("{}", err)
            from()
        }
        Log(err: log::SetLoggerError) {
            description(err.description())
            display("{}", err)
            from()
        }
        Log4rs(err: log4rs::config::Errors) {
            description(err.description())
            display("{}", err)
            from()
        }
    }
}

#[derive(Serialize, Deserialize)]
struct VodData {
    messages:      Vec<serde_json::Value>,
    end_timestamp: i64,
}

header! { (ClientID, "Client-ID") => [String] }

// https://api.twitch.tv/v5/videos/179617422/comments?content_offset_seconds=52
// client-id:jzkbprff40iqj646a697cyrvl0zt2m6
fn download(vod_id: i32) -> Result<VodData, Error> {
    info!("VOD chat log download started");

    let ssl = hyper_native_tls::NativeTlsClient::new().unwrap();
    let connector = hyper::net::HttpsConnector::new(ssl);
    let client = hyper::client::Client::with_connector(connector);

    let res = client.get(&format!("https://api.twitch.tv/kraken/videos/v{}", vod_id))
        .header(ClientID(CID.to_owned()))
        .send()?;

    if res.status != hyper::Ok {
        return Err(Error::MyError(format!("VOD info API status {}", res.status)))
    }

    let vod_info = match serde_json::from_reader(res) {
        Ok(serde_json::Value::Object(map)) => map,
        _ => return Err(Error::MyError(format!("got bad VOD info response")))
    };
    if vod_info.contains_key("error") {
        return Err(Error::MyError(format!("got an error in VOD info response: {}", vod_info["error"])))
    }

    let video_len = vod_info["length"].as_i64().ok_or_else(|| Error::MyError(format!("length not i64")))?;

    let mut messages = Vec::new();

    let mut last_downloaded_timestamp = 0.0;
    let mut next_cursor_string = String::new();

    'chunk_loop: loop {
        println!("Downloading VOD chat log chunk: {:.1}%", (last_downloaded_timestamp*100.)/((video_len+1) as f64));

        let get_chunk = |next_cursor_string : &str| {
            let res = client.get(&format!("https://api.twitch.tv/v5/videos/{}/comments{}", vod_id, next_cursor_string))
                .header(ClientID(CID.to_owned()))
                .send()?;

            if res.status != hyper::Ok {
                return Err(Error::MyError(format!("rechat-messages API request status {}", res.status)));
            }
            let v: serde_json::Value = serde_json::from_reader(res)?;
            Ok(v)
        };

        for attempt in 0..CHUNK_ATTEMPTS {
            if attempt > 0 {
                thread::sleep(std::time::Duration::from_secs(CHUNK_ATTEMPT_SLEEP));
                println!("An error occurred. Retrying (attempt {})", attempt+1);
            }

            match get_chunk(&next_cursor_string) {
                Err(e) => {
                    error!("VOD chat log chunk download error: {}", e);
                    continue;
                },
                Ok(data) => {
                    let data = data.as_object().ok_or_else(|| Error::MyError(format!("chunk response not map")))?;

                    let chunk_data = data["comments"].as_array().ok_or_else(||Error::MyError(format!("chunk data not array")))?;

                    for m in chunk_data {
                        let content_type = m["content_type"].as_str().ok_or_else(|| Error::MyError(format!("bad message: {}", m)))?;
                        if content_type != "video" {
                            println!("unknown rechat content_type {}", content_type);
                            continue;
                        }
                        let content_id = m["content_id"].as_str().ok_or_else(|| Error::MyError(format!("bad message: {}", m)))?;
                        if content_id != vod_id.to_string() {
                            println!("unknown rechat content_id {}", content_id);
                            continue;
                        }
                        let source = m["source"].as_str().ok_or_else(|| Error::MyError(format!("bad message: {}", m)))?;
                        if source != "chat" {
                            continue;
                        }
                        let state = m["state"].as_str().ok_or_else(|| Error::MyError(format!("bad message: {}", m)))?;
                        if state != "published" {
                            println!("unknown rechat state {}", state);
                            continue;
                        }

                        let timestamp = m["content_offset_seconds"].as_f64().ok_or_else(|| Error::MyError(format!("bad message: {}", m)))?;
                        last_downloaded_timestamp = timestamp;
                        if timestamp >= (video_len + 1) as f64 {
                            break 'chunk_loop;
                        }

                        messages.push(m.clone());
                    }

                    match data.get("_next") {
                        Some(next) => {
                            next_cursor_string = format!("?cursor={}", next.as_str().ok_or_else(|| Error::MyError(format!("bad _next value: {}", next)))?);
                            continue 'chunk_loop;
                        },
                        None => {
                            break 'chunk_loop;
                        }
                    }
                }
            };
        }

        return Err(Error::MyError(format!("max retry attempts exceeded")));
    }

    info!("VOD chat log download done!");
    Ok(VodData {
        messages: messages,
        end_timestamp: (video_len + 1)*1000,
    })
}


struct RunOnDrop<F: FnOnce()>(Option<F>);

impl<F: FnOnce()> RunOnDrop<F> {
    fn new(f: F) -> RunOnDrop<F> {
        RunOnDrop(Some(f))
    }
}

impl<F: FnOnce()> Drop for RunOnDrop<F> {
    fn drop(&mut self) {
        self.0.take().unwrap()();
    }
}


#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MyMessageTag {
    RendererOut, RendererErr, Encoder
}

pub enum MyMessage {
    String(String),
    Unit,
    EOF,
}


#[link(name = "encoder", kind = "static")]
extern {
    fn recorder_get_error() -> *const libc::c_char;

    fn recorder_init(rgb_path: *const libc::c_char,
                     a_path:   *const libc::c_char,
                     width:    libc::c_uint,
                     height:   libc::c_uint,
                     fps:      libc::c_uint) -> libc::c_int;

    fn recorder_loop_once(rgb_data: *const libc::uint8_t,
                          a_data:   *const libc::uint8_t,
                          next_pts: libc::int64_t) -> libc::c_int;

    fn recorder_cleanup() -> libc::c_int;
}

fn encoder(tag: MyMessageTag,
           settings: Settings,
           rgb_mp4_pathstr: String,
           a_mp4_pathstr: String,
           pts_times: Vec<i64>,
           buffer: mpsc::Receiver<imagefmt::Image<u8>>,
           progress: mpsc::Sender<(MyMessageTag, MyMessage)>) {

    thread::Builder::new().name(format!("encoder_{:?}", tag)).spawn(move || {
        let _destructor = RunOnDrop::new(|| {
            progress.send((tag, MyMessage::EOF)).unwrap();
        });

        let panic_on_recorder_failure = |ret: libc::c_int| {
            if ret < 0 {
                let c_error = unsafe { std::ffi::CStr::from_ptr(recorder_get_error()) };
                let error = c_error.to_string_lossy();
                let error = format!("encoder error: {}", error);
                progress.send((tag, MyMessage::String(error.to_owned()))).unwrap();
                panic!("{}", error);
            }
        };

        let c_rgb_mp4_pathstr = std::ffi::CString::new(rgb_mp4_pathstr).unwrap();
        let c_a_mp4_pathstr = std::ffi::CString::new(a_mp4_pathstr).unwrap();
        panic_on_recorder_failure(
            unsafe { recorder_init(c_rgb_mp4_pathstr.as_ptr(),
                                   if settings.render_alpha { c_a_mp4_pathstr.as_ptr() } else { std::ptr::null() },
                                   settings.width, settings.height, settings.fps) }
        );

        let _destructor = RunOnDrop::new(|| {
            panic_on_recorder_failure(
                unsafe { recorder_cleanup() }
            );
        });

        let frame_width = settings.width as usize;
        let frame_height = settings.height as usize;

        let mut data_rgb = Vec::<u8>::new();
        let mut data_a   = Vec::<u8>::new();
        if settings.render_alpha {
            data_rgb.resize(frame_width * frame_height * 3, 0);
            data_a  .resize(frame_width * frame_height * 1, 0);
        }

        for (&next_pts_time, msg_image) in pts_times[1..].iter().zip(buffer) {
            assert_eq!(msg_image.w, frame_width);
            assert_eq!(msg_image.h, frame_height);

            let (ptr_rgb, ptr_a) = {
                if settings.render_alpha {
                    assert_eq!(msg_image.fmt, imagefmt::ColFmt::RGBA);
                    assert_eq!(msg_image.buf.len(), msg_image.w * msg_image.h * 4);
                    for i in 0..frame_width * frame_height {
                        data_rgb[i*3 + 0] = msg_image.buf[i*4 + 0];
                        data_rgb[i*3 + 1] = msg_image.buf[i*4 + 1];
                        data_rgb[i*3 + 2] = msg_image.buf[i*4 + 2];
                        data_a  [i]       = msg_image.buf[i*4 + 3];
                    }
                    (data_rgb.as_ptr(), data_a.as_ptr())
                } else {
                    assert_eq!(msg_image.fmt, imagefmt::ColFmt::RGB);
                    assert_eq!(msg_image.buf.len(), msg_image.w * msg_image.h * 3);
                    (msg_image.buf.as_ptr(), std::ptr::null())
                }
            };

            panic_on_recorder_failure(
                unsafe { recorder_loop_once(ptr_rgb, ptr_a, next_pts_time) }
            );
            progress.send((tag, MyMessage::Unit)).unwrap();
        }
    }).unwrap();
}

fn b64decpipe(tag: MyMessageTag,
              settings: Settings,
              from: Box<Read + Send>,
              to: mpsc::SyncSender<imagefmt::Image<u8>>,
              progress: mpsc::Sender<(MyMessageTag, MyMessage)>) {

    thread::Builder::new().name(format!("b64decpipe_{:?}", tag)).spawn(move || {
        let _destructor = RunOnDrop::new(|| {
            progress.send((tag, MyMessage::EOF)).unwrap();
        });

        let mut buf_reader = BufReader::new(from);
        loop {
            let mut buffer = String::new();
            if buf_reader.read_line(&mut buffer).unwrap() == 0 {
                info!("{:?} handle has closed", tag);
                break;
            } else {
                let data = buffer.from_base64().expect("base64 decode");
                let col_fmt = if settings.render_alpha { imagefmt::ColFmt::RGBA } else { imagefmt::ColFmt::RGB };
                let image = imagefmt::png::read(&mut std::io::Cursor::new(data), col_fmt).expect("renderer produced invalid png");
                to.send(image).unwrap();
                progress.send((tag, MyMessage::Unit)).unwrap();
            }
        }
    }).unwrap();
}

fn readlines(tag: MyMessageTag, from: Box<Read + Send>, to: mpsc::Sender<(MyMessageTag, MyMessage)>) {
    thread::Builder::new().name(format!("readlines_{:?}", tag)).spawn(move || {
        let _destructor = RunOnDrop::new(|| {
            to.send((tag, MyMessage::EOF)).unwrap();
        });

        let mut buf_reader = BufReader::new(from);
        loop {
            let mut buffer = String::new();
            if buf_reader.read_line(&mut buffer).unwrap() == 0 {
                info!("{:?} handle has closed", tag);
                break;
            } else {
                to.send((tag, MyMessage::String(buffer))).unwrap();
            }
        }
    }).unwrap();
}

fn canon_maybe(p: &PathBuf) -> String {
    match std::fs::canonicalize(p) {
        Ok(p) => format!("{}", p.display()),
        Err(_) => format!("{}", p.display()),
    }
}

fn setup_logging(log_path: &Path) -> Result<log4rs::Handle, Error> {
    use log::LogLevelFilter;
    use log4rs::append::console::ConsoleAppender;
    use log4rs::append::file::FileAppender;
    use log4rs::filter::threshold::ThresholdFilter;
    use log4rs::encode::pattern::PatternEncoder;
    use log4rs::config::{Appender, Config, Root};

    let stdout_appender = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new("{l}: {m}{n}")))
        .build();

    let file_appender = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new("{d} {l} {T} - {m}{n}")))
        .append(false)
        .build(log_path)?;

    let config = Config::builder()
        .appender(Appender::builder()
                  .filter(Box::new(ThresholdFilter::new(LogLevelFilter::Error)))
                  .build("stdout", Box::new(stdout_appender)))
        .appender(Appender::builder().build("file", Box::new(file_appender)))
        .build(Root::builder().appender("stdout").appender("file").build(LogLevelFilter::Debug))?;

    Ok(log4rs::init_config(config)?)
}

fn readline(prompt: &str) -> String {
    print!("{}", prompt);
    let _ = std::io::stdout().flush();

    let mut input = String::new();
    let _ = std::io::stdin().read_line(&mut input);

    input.trim().to_owned()
}

fn the_main() -> i32 {
    let auto_mode = std::env::args().nth(1).is_some();

    let _destructor = RunOnDrop::new(move || {
        if !auto_mode {
            let _ = readline("Press ENTER to exit. ");
        }
    });

    let safety_net_log_path = std::sync::Arc::new(std::sync::Mutex::new(None));

    // SAFETY NET
    std::panic::set_hook({
        let main_thread = thread::current();
        let hook_log_path_handle = safety_net_log_path.clone();
        Box::new(move |info| {
            let backtrace = backtrace::Backtrace::new();

            let msg = match info.payload().downcast_ref::<&'static str>() {
                Some(s) => *s,
                None => match info.payload().downcast_ref::<String>() {
                    Some(s) => &**s,
                    None => "Box<Any>",
                }
            };

            error!("{} [panic]", msg);
            info!("Backtrace:\n{:?}", backtrace);

            if thread::current().id() == main_thread.id() {
                let log_path_info = hook_log_path_handle.lock().ok()
                    .and_then(|guard| (*guard).as_ref()
                              .map(|path: &std::path::PathBuf|
                                   format!(" Debug log saved at \"{}\"", path.display())))
                    .unwrap_or_else(|| "".to_owned());

                println!("---\nOops! I crashed.{}\n", log_path_info);
            }
        })
    });

    // PREFLIGHT CHECKS

    let settings_path = Path::new(SETTINGS_JSON);
    let settings: Settings = match File::open(settings_path) {
        Err(e) => {
            println!("Error opening settings file \"{}\": {}", SETTINGS_JSON, e);
            return 1;
        },
        Ok(f) => match serde_json::from_reader(f) {
            Ok(s) => s,
            Err(e) => {
                println!("Error reading settings file \"{}\": {}", SETTINGS_JSON, e);
                return 1;
            },
        },
    };

    let phantomjs_path = std::fs::canonicalize(&settings.phantomjs_path).unwrap_or_else(|_| settings.phantomjs_path.clone());

    info!("phantomjs path: {}", phantomjs_path.display());

    if !phantomjs_path.exists() {
        println!("ERROR: phantomjs not found.");
        println!("Please download it, and set the \"phantomjs_path\" configuration");
        println!("in res/settings.json to the location of phantomjs.exe.\n");

        panic!("Required executable not found: \"{}\"", phantomjs_path.display());
    }

    // PROMPT

    let vod_id_str = match std::env::args().nth(1) {
        None => readline("Enter the VOD ID (e.g. 120993420): "),
        Some(x) => x,
    };
    let vod_id: i32 = match vod_id_str.parse() {
        Err(e) => {
            println!("\"{}\" is not a valid VOD id: {}", vod_id_str, e);
            return 1;
        },
        Ok(y) => y,
    };

    let mut log_path = settings.output_dir.clone();
    log_path.push(format!("{}.log", vod_id));
    let mut json_path = settings.output_dir.clone();
    json_path.push(format!("{}.json", vod_id));
    let mut rgb_mp4_path = settings.output_dir.clone();
    rgb_mp4_path.push(format!("{}.mp4", vod_id));
    let mut a_mp4_path = settings.output_dir.clone();
    a_mp4_path.push(format!("{}_alpha.mp4", vod_id));

    if rgb_mp4_path.exists() {
        if readline(&format!("\"{}\" already exists! Overwrite? (y/N) ", canon_maybe(&rgb_mp4_path))).to_lowercase() != "y" {
            println!("Not overwriting. Have a good day!");
            println!("");
            return 0;
        }
        let _ = std::fs::remove_file(&log_path);
        let _ = std::fs::remove_file(&rgb_mp4_path);
        let _ = std::fs::remove_file(&a_mp4_path);
    }

    setup_logging(&log_path).unwrap();
    let _ = safety_net_log_path.lock().map(|mut data| { *data = Some(log_path.clone()) });

    let (vod_data, pts_times) = (|| -> Result<(VodData, Vec<i64>), Error> {
        let vod_data = {
            if json_path.exists() {
                println!("Chat for VOD {} already downloaded", vod_id);
                let json_file = File::open(&json_path).unwrap();
                serde_json::from_reader(json_file).expect("saved VOD logs corrupted, delete the .json and .pts files and try again")
            } else {
                println!("Downloading chat logs for VOD {}", vod_id);
                let v = download(vod_id).expect("VOD chat log download failed");
                let mut json_file = File::create(&json_path).unwrap();
                serde_json::to_writer(&mut json_file, &v).unwrap();
                v
            }
        };

        let mut pts_times = Vec::with_capacity(vod_data.messages.len() + 2);
        pts_times.push(0);
        for m in &vod_data.messages {
            let time = m["content_offset_seconds"].as_f64().unwrap();
            pts_times.push((time*1000.) as i64);
        }
        pts_times.push(vod_data.end_timestamp);
        for i in 1..(pts_times.len()) {
            if pts_times[i] <= pts_times[i-1] {
                // ensure strict monotonicity
                pts_times[i] = pts_times[i-1] + 1;
            }
        }

        Ok((vod_data, pts_times))
    })().expect("VOD logs corrupted, delete the .json and .pts files and try again");

    let messages_len = vod_data.messages.len();

    println!("Starting encoding...");

    info!("{} messages loaded", messages_len);
    info!("Starting encode of {} messages; {}x{} @ {} fps",
          messages_len, settings.width, settings.height, settings.fps);

    let mut renderer_process = Command::new(phantomjs_path)
        .arg("res/runner.js")
        .arg(settings.width.to_string())
        .arg(settings.height.to_string())
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn().unwrap();

    let encode_start_time = std::time::Instant::now();

    let (progress_tx, progress_rx) = mpsc::channel();
    let (buffer_tx, buffer_rx) = mpsc::sync_channel(50);

    b64decpipe(MyMessageTag::RendererOut,
               settings.clone(),
               Box::new(renderer_process.stdout.take().expect("renderer stdout")),
               buffer_tx,
               progress_tx.clone());

    encoder(MyMessageTag::Encoder,
            settings.clone(),
            rgb_mp4_path.to_str().expect("bad mp4 path").to_owned(),
            a_mp4_path.to_str().expect("bad alpha mp4 path").to_owned(),
            pts_times,
            buffer_rx,
            progress_tx.clone());

    readlines(MyMessageTag::RendererErr,
              Box::new(renderer_process.stderr.take().expect("renderer stderr")),
              progress_tx.clone());

    drop(progress_tx);

    let mut renderer_stdin = renderer_process.stdin.take().expect("renderer stdin");

    let mut renderer_errs = 0;
    let mut encoder_errs = 0;
    let mut renderer_last_sent: i32 = -1;
    let mut encoder_last_seen: i32 = -1;

    let rendering_result = (|| -> Result<(), Error> {
        for rmsg in progress_rx.iter() {
            match rmsg {
                (MyMessageTag::RendererOut, MyMessage::Unit) => {
                    renderer_last_sent += 1;
                    match renderer_last_sent.cmp(&(messages_len as i32)) {
                        std::cmp::Ordering::Less => {
                            writeln!(renderer_stdin, "{}", vod_data.messages[renderer_last_sent as usize])?;
                            renderer_stdin.flush()?;
                        },
                        std::cmp::Ordering::Equal => {
                            writeln!(renderer_stdin, "null")?;
                            renderer_stdin.flush()?;
                        },
                        std::cmp::Ordering::Greater => {
                            return Err(Error::MyError(format!("Renderer kept outputting frames after input ceased")));
                        },
                    }
                },
                (MyMessageTag::RendererOut, MyMessage::EOF) => {
                    if renderer_last_sent != messages_len as i32 {
                        return Err(Error::MyError(format!("Renderer stdout disconnected after having sent {}/{} frames", renderer_last_sent+1, messages_len+1)));
                    }
                },

                (MyMessageTag::Encoder, MyMessage::Unit) => {
                    encoder_last_seen += 1;
                    println!("Encoding message: {}/{} ({:.1}%)",
                        encoder_last_seen+1, messages_len+1,
                        (((encoder_last_seen+1)*100) as f32)/((messages_len+1) as f32));
                },
                (MyMessageTag::Encoder, MyMessage::EOF) => {
                    if encoder_last_seen != messages_len as i32 {
                        return Err(Error::MyError(format!("Encoder thread disconnected after having seen {}/{} frames", encoder_last_seen+1, messages_len+1)));
                    }
                },

                (MyMessageTag::RendererErr, MyMessage::Unit) => unreachable!(),
                (MyMessageTag::RendererErr, MyMessage::EOF) => (),

                (tag, MyMessage::String(msg_str)) => {
                    if tag == MyMessageTag::RendererErr { renderer_errs += 1; }
                    if tag == MyMessageTag::Encoder { encoder_errs += 1; }
                    warn!("{:?}: {}", tag, msg_str.trim());
                },
            }
        };
        Ok(())
    })();

    let _ = rendering_result.as_ref().map_err(|e| {
        error!("{}", e);
        info!("Killing renderer due to error");
        let _ = renderer_process.kill();
    });

    drop(renderer_stdin);

    let renderer_retcode = renderer_process.wait().unwrap();
    info!("Renderer returned with code {}", renderer_retcode);

    println!("\n{}\n", String::from_iter(Iterator::take(Iterator::cycle(Some('=').into_iter()), 80)));

    match rendering_result {
        Ok(()) => {
            let elapsed = encode_start_time.elapsed();
            println!("Finished! ({}.{}s elapsed)", elapsed.as_secs(), elapsed.subsec_nanos() / 100_000_000);
            println!("");
            println!(" > MP4 output saved to:  {}", canon_maybe(&rgb_mp4_path));
            if settings.render_alpha {
                println!(" > Matte mask saved to:  {}", canon_maybe(&a_mp4_path));
            }
            println!("");
            println!("Additional stuff you may now delete:");
        },
        Err(_) => {
            println!("It looks like something went wrong...");
            println!("");
            let full_rgb_mp4_path = canon_maybe(&rgb_mp4_path);
            let full_a_mp4_path = canon_maybe(&a_mp4_path);
            if Path::new(&full_rgb_mp4_path).exists() {
                println!("Possibly broken MP4 saved to: {}", full_rgb_mp4_path);
                if settings.render_alpha && Path::new(&full_a_mp4_path).exists() {
                    println!("Matte mask saved to:          {}", full_a_mp4_path);
                }
                println!("");
            }
            println!("Stuff which might be useful for diagnosis:");
        },
    }
    println!(" - VOD chat saved to:    {}", canon_maybe(&json_path));
    println!(" - Debug log saved to:   {}", canon_maybe(&log_path));

    if renderer_errs > 0 || encoder_errs > 0 {
        println!("");
        if renderer_errs > 0 {
            println!("{} renderer issues occurred (probably means some messages were dropped!)", renderer_errs);
        }
        if encoder_errs > 0 {
            println!("{} encoder issues occurred.", encoder_errs);
        }
        println!("Check debug log for details.");
    }
    println!("");
    0
}

fn main() {
    println!("rechat-record v{}: a Twitch chat replay encoder", env!("CARGO_PKG_VERSION_MAJOR"));
    println!("Copyright (C) 2017 hexpointer <hexptr@gmx.com>");
    println!("");
    println!("This is free software, licensed under GPLv3. See COPYING for details.");
    println!("");
    println!("{}", String::from_iter(Iterator::take(Iterator::cycle(Some('-').into_iter()), 80)));
    println!("");
    let ret = the_main();
    std::process::exit(ret);
}
