/*
 * Copyright (c) 2003-2014 FFmpeg project
 * Copyright (c) 2017 hexpointer <hexptr@gmx.com>
 *
 * This file is part of rechat-record.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* adapted from various examples including muxing.c, vf_fps.c */

#include <stdio.h>

#include <libavutil/avassert.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>

static unsigned stream_fps;

typedef struct {
    AVFormatContext *oc;
    AVCodecContext *enc;
    AVFrame *enc_frame;
    struct SwsContext *sws_ctx;
    int frames_out;
} Context;

static Context *ctx_rgb;
static Context *ctx_a;

#define ERRORSZ (80 + AV_ERROR_MAX_STRING_SIZE)
static char error_buf[ERRORSZ];

const char *recorder_get_error()
{
    return error_buf;
}

static int context_init(Context *ctx, unsigned width, unsigned height, enum AVPixelFormat src_pix_fmt, const char *output_filename)
{
    int ret;
    AVCodec *video_codec;
    AVStream *ost;
    AVDictionary *private_opts = NULL;

    /* allocate the output media context */
    ret = avformat_alloc_output_context2(&ctx->oc, NULL, "mp4", output_filename);
    if (ret < 0) {
        snprintf(error_buf, ERRORSZ, "Could not allocate MP4 output context: %s", av_err2str(ret));
        return ret;
    }

    /* Add the video streams using the default format codecs
     * and initialize the codecs. */
    av_assert0(ctx->oc->oformat->video_codec != AV_CODEC_ID_NONE);
    av_assert0(!(ctx->oc->oformat->flags & AVFMT_NOFILE));

    /* find the encoder */
    video_codec = avcodec_find_encoder(ctx->oc->oformat->video_codec);
    av_assert0(video_codec);

    /* add a new output stream */
    ost = avformat_new_stream(ctx->oc, NULL);
    if (!ost) {
        snprintf(error_buf, ERRORSZ, "Could not allocate stream");
        return -1;
    }
    ost->id = ctx->oc->nb_streams-1;
    ctx->enc = avcodec_alloc_context3(video_codec);
    if (!ctx->enc) {
        snprintf(error_buf, ERRORSZ, "Could not alloc an encoding context");
        return -1;
    }

    av_assert0((video_codec->type) == AVMEDIA_TYPE_VIDEO);

    ctx->enc->codec_id = video_codec->id;
    ctx->enc->pix_fmt  = AV_PIX_FMT_YUV420P;

    ctx->enc->gop_size = stream_fps * 10;
    /* Resolution must be a multiple of two. */
    ctx->enc->width    = width;
    ctx->enc->height   = height;
    /* timebase: This is the fundamental unit of time (in seconds) in terms
     * of which frame timestamps are represented. For fixed-fps content,
     * timebase should be 1/framerate and timestamp increments should be
     * identical to 1. */
    ost->time_base = ctx->enc->time_base = (AVRational){ 1, stream_fps };

    /* Some formats want stream headers to be separate. */
    if (ctx->oc->oformat->flags & AVFMT_GLOBALHEADER)
        ctx->enc->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    ctx->enc->thread_count = 2;
    ctx->enc->max_b_frames = 0;
    ctx->enc->refs         = 1;
    ctx->enc->me_cmp       = FF_CMP_SAD;

    av_dict_set(&private_opts, "preset",      "medium", 0);
    av_dict_set(&private_opts, "tune",        "stillimage", 0);
    av_dict_set(&private_opts, "crf",         "23", 0);
    av_dict_set(&private_opts, "direct-pred", "none", 0);
    av_dict_set(&private_opts, "weightp",     "0", 0);
    av_dict_set(&private_opts, "mixed-refs",  "0", 0);

    /* Now that all the parameters are set, we can open the
     * video codecs and allocate the necessary encode buffers. */

    /* open the codec */
    ret = avcodec_open2(ctx->enc, video_codec, &private_opts);
    if (ret < 0) {
        snprintf(error_buf, ERRORSZ, "Could not open video codec: %s", av_err2str(ret));
        return ret;
    }
    av_dict_free(&private_opts);

    /* copy the stream parameters to the muxer */
    ret = avcodec_parameters_from_context(ost->codecpar, ctx->enc);
    if (ret < 0) {
        snprintf(error_buf, ERRORSZ, "Could not copy the stream parameters: %s", av_err2str(ret));
        return ret;
    }

    /* open the output file, if needed */
    ret = avio_open(&ctx->oc->pb, output_filename, AVIO_FLAG_WRITE);
    if (ret < 0) {
        snprintf(error_buf, ERRORSZ, "Could not open '%s': %s", output_filename,
                av_err2str(ret));
        return ret;
    }

    /* Write the stream header, if any. */
    ret = avformat_write_header(ctx->oc, NULL);
    if (ret < 0) {
        snprintf(error_buf, ERRORSZ, "Error occurred when opening output file: %s",
                av_err2str(ret));
        return ret;
    }

    /* allocate frame */

    ctx->enc_frame = av_frame_alloc();
    if (!ctx->enc_frame) {
        snprintf(error_buf, ERRORSZ, "Could not allocate frame");
        return -1;
    }

    ctx->enc_frame->format = ctx->enc->pix_fmt;
    ctx->enc_frame->width  = ctx->enc->width;
    ctx->enc_frame->height = ctx->enc->height;

    /* allocate the buffers for the frame data */
    ret = av_frame_get_buffer(ctx->enc_frame, 32);
    if (ret < 0) {
        snprintf(error_buf, ERRORSZ, "Could not allocate frame data: %s", av_err2str(ret));
        return ret;
    }

    /* as we only generate a INPUT_PIX_FMT picture, we must convert it
     * to the codec pixel format if needed */
    ctx->sws_ctx = sws_getContext(width, height,
            src_pix_fmt,
            ctx->enc->width, ctx->enc->height,
            ctx->enc->pix_fmt,
            0, NULL, NULL, NULL);
    if (!ctx->sws_ctx) {
        snprintf(error_buf, ERRORSZ,
                "Could not initialize the conversion context");
        return -1;
    }

    ctx->frames_out = 0;

    return 0;
}

int recorder_init(const char *rgb_filename, const char *a_filename, unsigned width, unsigned height, unsigned fps)
{
    int ret;

    stream_fps = fps;

    /* Initialize libavcodec, and register all codecs and formats. */
    av_log_set_level(AV_LOG_WARNING);
    av_register_all();
    avcodec_register_all();

    ctx_rgb = malloc(sizeof(Context));
    ret = context_init(ctx_rgb, width, height, AV_PIX_FMT_RGB24, rgb_filename);
    if (ret < 0) {
        return ret;
    }

    if (a_filename) {
        ctx_a = malloc(sizeof(Context));
        ret = context_init(ctx_a, width, height, AV_PIX_FMT_GRAY8, a_filename);
        if (ret < 0) {
            return ret;
        }
    }

    return 0;
}

static int encode_frame(Context *ctx, AVFrame *frame, int64_t next_pts)
{
    AVPacket pkt = { 0 };
    int ret;
    int got_output_frame = 0;
    int i, delta;

    av_init_packet(&pkt);

    if (frame) {
        delta = av_rescale_q(next_pts, (AVRational){1, 1000}, ctx->enc->time_base) - ctx->frames_out;
    } else {
        /* flush */
        delta = 1;
    }

    for (i = 0; i < delta; i++) {
        if (frame) {
            frame->pts = ctx->frames_out++;
        }

        /* encode the image */
        ret = avcodec_send_frame(ctx->enc, frame);
        if (ret < 0) {
            snprintf(error_buf, ERRORSZ, "Error encoding video frame: %s", av_err2str(ret));
            return ret;
        }

        while (1) {
            ret = avcodec_receive_packet(ctx->enc, &pkt);
            if (( frame && ret == AVERROR(EAGAIN)) ||
                (!frame && ret == AVERROR_EOF)) {
                break;
            }
            if (ret < 0) {
                snprintf(error_buf, ERRORSZ, "Error receiving video packet: %s", av_err2str(ret));
                return ret;
            }

            av_packet_rescale_ts(&pkt, ctx->enc->time_base, ctx->oc->streams[0]->time_base);
            pkt.stream_index = ctx->oc->streams[0]->index;

            /* Write the compressed frame to the media file. */
            ret = av_interleaved_write_frame(ctx->oc, &pkt);
            if (ret < 0) {
                snprintf(error_buf, ERRORSZ, "Error while writing video frame: %s", av_err2str(ret));
                return ret;
            }
            av_packet_unref(&pkt);
        }
    }

    return 0;
}

int recorder_loop_once(const uint8_t *data_rgb, const uint8_t *data_a, int64_t next_pts)
{
    int ret;

    const uint8_t *slices[] = {data_rgb, NULL, NULL, NULL};
    int slice_strides[] = {ctx_rgb->enc->width*3, 0, 0, 0};
    sws_scale(ctx_rgb->sws_ctx, slices, slice_strides,
              0, ctx_rgb->enc->height, ctx_rgb->enc_frame->data, ctx_rgb->enc_frame->linesize);

    ret = encode_frame(ctx_rgb, ctx_rgb->enc_frame, next_pts);
    if (ret < 0) {
        return ret;
    }

    if (ctx_a) {
        const uint8_t *slices[] = {data_a, NULL, NULL, NULL};
        int slice_strides[] = {ctx_a->enc->width, 0, 0, 0};
        sws_scale(ctx_a->sws_ctx, slices, slice_strides,
                  0, ctx_a->enc->height, ctx_a->enc_frame->data, ctx_a->enc_frame->linesize);

        ret = encode_frame(ctx_a, ctx_a->enc_frame, next_pts);
        if (ret < 0) {
            return ret;
        }
    }

    return 0;
}

static int context_cleanup(Context *ctx)
{
    int ret;

    // flush
    ret = encode_frame(ctx, NULL, 0);
    if (ret < 0) {
        return ret;
    }

    /* Write the trailer, if any. The trailer must be written before you
     * close the CodecContexts open when you wrote the header; otherwise
     * av_write_trailer() may try to use memory that was freed on
     * av_codec_close(). */
    av_write_trailer(ctx->oc);

    /* Close each codec. */
    avcodec_close(ctx->enc);
    avcodec_free_context(&ctx->enc);
    av_frame_free(&ctx->enc_frame);

    sws_freeContext(ctx->sws_ctx);

    /* Close the output file. */
    avio_closep(&ctx->oc->pb);

    /* free the stream */
    avformat_free_context(ctx->oc);

    return 0;
}

int recorder_cleanup()
{
    int ret;

    ret = context_cleanup(ctx_rgb);
    free(ctx_rgb);
    if (ret < 0) {
        return ret;
    }

    if (ctx_a) {
        ret = context_cleanup(ctx_a);
        free(ctx_a);
        if (ret < 0) {
            return ret;
        }
    }

    return 0;
}
